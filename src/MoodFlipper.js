import React, { useState } from 'react';

function MoodFlipper() {
    const moods = {
        happy: '😊',
        sad: '😢',
        excited: '🥳',
        thoughtful: '🤔'
    };
    const [mood, setMood] = useState(moods.happy); // Start with the happy mood

    const changeMood = () => {
        const newMood = Object.values(moods)[Math.floor(Math.random() * Object.keys(moods).length)];
        setMood(newMood);
    };

    return (
        <div>
            <h1>Mood Flipper</h1>
            <p>Mood: {mood}</p>
            <button onClick={changeMood}>Change Mood</button>
        </div>
    );
}

export default MoodFlipper;
