import React from 'react';
import logo from './logo.svg';  // Ensure the logo image is correctly imported
import './App.css';
import MoodFlipper from './MoodFlipper';  // Importing the MoodFlipper component

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <MoodFlipper />  {/* Using MoodFlipper component */}
        <p>Enjoy Forever</p>  {/* Corrected Text */}
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
      <footer>
        <p>Brought to you by Team 12</p>
      </footer>
    </div>
  );
}

export default App;

